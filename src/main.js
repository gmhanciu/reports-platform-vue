// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import VueRouter from "vue-router";
import App from "./App";

// router setup
import routes from "./routes/routes";

// Plugins
import GlobalComponents from "./globalComponents";
import GlobalDirectives from "./globalDirectives";
import Notifications from "./components/NotificationPlugin";

// MaterialDashboard plugin
import MaterialDashboard from "./material-dashboard";

import Chartist from "chartist";

// Other imports
import axios from 'axios';

// configure router
const router = new VueRouter({
    routes, // short for routes: routes
    linkExactActiveClass: "nav-item active"
});

Vue.use(VueRouter);
Vue.use(MaterialDashboard);
Vue.use(GlobalComponents);
Vue.use(GlobalDirectives);
Vue.use(Notifications);

Vue.prototype.$Chartist = Chartist;
Vue.prototype.$axios = axios;

Vue.prototype.$__PHP_CONFIG__ = '';

// let __PHP_CONFIG__ = '';

axios.post('/reports-platform/init-config')
    .then((response) => {
        // console.log(response);
        return response.data;
    })
    .then((data) => {
        if (data)
        {
            // __PHP_CONFIG__ = data;
            Vue.prototype.$__PHP_CONFIG__ = data;
        }
        else
        {
            throw 'Initialize config could not be loaded!';
        }
    });

/* eslint-disable no-new */
new Vue({
    el: "#app",
    render: h => h(App),
    router,
    data: {
        // __PHP_CONFIG__: __PHP_CONFIG__,
        Chartist: Chartist
    }
});
